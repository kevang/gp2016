import os
import subprocess
import tokenization
import unittest


class TokenizerTest(unittest.TestCase):

    def test_tokenizer(self):
        self.assertEqual(
            tokenization.tokenize('"Hark! A vagrant," he said.'),
            ['hark', 'a', 'vagrant', 'he', 'said'])


class WordfreqTest(unittest.TestCase):

    def test_wordfreq(self):
        output = subprocess.check_output(('python3', 'wordfreq.py',
                                         'documents/moby11.txt'),
                                         universal_newlines=True)
        self.assertEqual(output, '''\
the 14512
of 6662
and 6464
a 4770
to 4692
in 4187
that 3091
it 2548
his 2529
i 2126
''')


class WordfreqStopwordsTest(unittest.TestCase):

    def test_wordfreq(self):
        output = subprocess.check_output(('python3', 'wordfreq_stopwords.py',
                                         'documents/janey11.txt'),
                                         universal_newlines=True)
        self.assertEqual(output, '''\
would 668
one 601
said 583
mr 544
could 505
like 405
rochester 366
jane 350
well 348
little 342
''')


class WordfreqTextTest(unittest.TestCase):

    def test_wordfreq_text(self):
        self.assertTrue(os.path.isfile('wordfreq.txt'))
        with open('wordfreq.txt') as f:
            self.assertGreater(len(f.read().strip()), 42,
                               'wordfreq.txt should contain an answer of at '
                               'least 1-2 sentences')


class SearchTest(unittest.TestCase):

    def test_search_love(self):
        output = subprocess.check_output(('python3', 'search.py', 'love'),
                                         universal_newlines=True)
        lines = output.splitlines()
        documents = [line.split()[0] for line in lines]
        self.assertEqual(documents, ['pandp12.txt', 'janey11.txt',
                                     'alice30.txt', 'moby11.txt',
                                     '2000010.txt'],
                         'Results not in the expected order for search word '
                         '"love"')

    def test_search_sea(self):
        output = subprocess.check_output(('python3', 'search.py', 'sea'),
                                         universal_newlines=True)
        lines = output.splitlines()
        documents = [line.split()[0] for line in lines]
        self.assertEqual(documents, ['2000010.txt', 'moby11.txt',
                                     'alice30.txt', 'janey11.txt',
                                     'pandp12.txt'],
                         'Results not in the expected order for search word '
                         '"sea"')

    def test_search_confusion(self):
        output = subprocess.check_output(('python3', 'search.py', 'confusion'),
                                         universal_newlines=True)
        lines = output.splitlines()
        documents = [line.split()[0] for line in lines]
        self.assertEqual(documents, ['alice30.txt', 'pandp12.txt',
                                     'janey11.txt', '2000010.txt',
                                     'moby11.txt'],
                         'Results not in the expected order for search word '
                         '"confusion"')
