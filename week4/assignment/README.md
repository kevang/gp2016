Assignment for week 4
=====================

Step 1: Get the starter code
----------------------------

On the command line, go into your local copy of the course repository. Update
it using the following command.

    git pull

This adds a directory `week4`, containing slides and assignment files.

In `week4/assignment`, you will find a subdirectory called `documents`. It
contains a small sample of five novels from
[Project Gutenberg](https://www.gutenberg.org): Alice in Wonderland, Jane Eyre,
Moby Dick, Pride and Prejudice and Twenty Thousand Leagues Under the Sea.

Step 2: Implement a simple tokenizer (3 points)
-----------------------------------------------

Create a module `tokenization` and add a function `tokenize` to it. It should
take a string as argument and return the words in this string as a list.
Punctuation should be discarded for this exercise. You can do this in the
following way:

1. Convert the input string to lower case
2. Replace every non-alphabetic character in the string by a space `' '` (hint:
   use the [isalpha](https://docs.python.org/3/library/stdtypes.html?highlight=isalpha#str.isalpha)
   method)
3. Use the [split](https://docs.python.org/3/library/stdtypes.html?highlight=isalpha#str.split)
   method to get the list of words separated by whitespace

Step 3: Output the ten most frequent words in the corpus (2 points)
-------------------------------------------------------------------

Write a program `wordfreq.py` that takes one command-line argument. This
argument is the path to a file, e.g. `documents/janey11.txt`. The program
should output the 10 most frequent words in the given text file, one word per
line, followed by a space, followed by its frequency (i.e. the number of times
is occurs).

Step 4: Filter stopwords (2 points)
-----------------------------------

Write a program `wordfreq_stopwords.py` that is like `wordfreq.py` but does not
count stopwords. Use the list in the file `stopwords.txt` to decide which words
are stopwords.

Step 5: Examine the outputs (1 point)
-------------------------------------

Try both programs out on the documents in our example corpus. What do you
notice about the output? What does the output of `wordfreq_stopwords.py` tell
you that the output of `wordfreq.py` does not? Describe this in 2–4 sentences
in a text file called `wordfreq.txt`.

Step 6: Implement a simple search with tf scoring (2 points)
------------------------------------------------------------

Write a program `search.py` that takes one command-line argument. This argument
is a search word. The program should give the list of *all* documents in our
little corpus of novels, ordered by their “relevance” for the given search
word. We are also interested in search terms that are so common that they occur
in most or all documents. tf-idf is not useful in this case because it is 0 for
terms that occur in all document, no matter the document. Thus, we will use
pure tf scores to get an idea of the relevance.

How to calculate the tf score for a word t in document d:

1. Determine the frequency of each word in d 
2. Divide the frequency of t in d by the frequency of the most frequent word
   in d

Output the file names of all documents, one per line, followed by a space,
followed by the tf score. Order them by decreasing tf score so that the “most
relevant” document appears first.

Step 7: Run the unit tests
--------------------------

From the `week4/assignment` directory, run the following command:

    python3 -m unittest

If you completed steps 2–5 correctly, all tests should pass. Note that
additional tests may be used to grade your work.

Step 8: Submit your work
------------------------

From the `week4/assignment` directory, run `git status` to see all the files
you modified and created. Add them all and commit the changes with a
meaningful message. Push them to your private repository using this command:

    git push mine master

Go to https://bitbucket.org/USERNAME/gp2016/src/master/week4/assignment/
(substituting your own username) and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

Grading
-------

Homework is graded twice a week, usually on Tuesdays and once during the
weekend.

You may submit up to two attempts. The one with the better grade is counted.

You are strongly encouraged to finish the assignment and submit as early as
possible. This way, you will receive a grade for your first attempt and still
have time to submit another attempt if desired.

The last possible date to submit an attempt is *Tuesday, March 8, 2016*.

*Happy hacking!*
