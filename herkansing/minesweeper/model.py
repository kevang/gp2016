import random


class MinesweeperModel:

    """
    The model representing the state of the minefield. Each tile is identified
    by a pair of coordinates (x, y) where x indicates the column and y
    indicates the row. For example, the tiles on a 3x3 minefield have these
    coordinates:

    (0, 0) (1, 0) (2, 0)
    (0, 1) (1, 1) (2, 1)
    (0, 2) (1, 2) (2, 2)
    """

    def __init__(self, width, height, mines):
        """
        Creates a new minesweeper model with the specified width (number of
        tiles in a row), height (number of tiles in a column) and number of
        mines. The mines are positioned on the minefield randomly.
        """
        raise NotImplementedError()

    def get_status(self, x, y):
        """
        Returns the status of the tile at coordinates (x, y).

        For unrevealed tiles, returns:
        'mine' if the game is lost and the tile is mined
        'incorrect' if the game is lost and the tile is flagged and not mined
        'flag' if the game is won and the tile is mined
        'flag' if the tile is flagged
        'blank' otherwise

        For revealed tiles, returns:
        'exploded' if the tile is mined
        'Nmines' where N is the number of mines on surrounding tiles
        """
        raise NotImplementedError()

    def reveal(self, x, y):
        """
        Reveals the tile at the given coordinates.
        """
        raise NotImplementedError()

    def flag(self, x, y):
        """
        Marks the tile at coordinates (x, y) flagged.
        """
        raise NotImplementedError()

    def unflag(self, x, y):
        """
        Marks the tile at coordinates (x, y) no longer flagged.
        """
        raise NotImplementedError()

    def is_game_lost(self):
        """
        Returns True if the game is lost, i.e. a mined tile has been revealed.
        """
        raise NotImplementedError()

    def is_game_won(self):
        """
        Returns True if the game is won, i.e. all non-mined tiles have been
        revealed.
        """
        raise NotImplementedError()
