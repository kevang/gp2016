import view


class MinesweeperController:

    def __init__(self, model, view):
        self._model = model
        self._view = view

    def run(self):
        self._view.show()
        self._view.update()
        while True:
            action = self._view.get_action()
            if isinstance(action, view.RevealAction):
                self._model.reveal(action.x, action.y)
            elif isinstance(action, view.FlagAction):
                self._model.flag(action.x, action.y)
            elif isinstance(action, view.UnflagAction):
                self._model.unflag(action.x, action.y)
            elif isinstance(action, view.QuitAction):
                break
            self._view.update()
