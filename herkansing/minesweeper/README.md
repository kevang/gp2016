Resit assignment 1: Minesweeper
===============================

The goal in this assignment is to create a basic implementation of the classic
video game Minesweeper.

There is a rectangular board with a number of tiles. The player can reveal
tiles by left-clicking on them. But some tiles are mined. If the player reveals
a mined tile, she loses. If the player reveals all non-mined tiles without
revealing a mined one, she wins.

If the user knows a certain unrevealed tile has a mine, she can flag the tile
by right-clicking it. This tells her how many mines she has discovered and
protects her from accidentally revealing it.

Revealed tiles give hints: they indicate how many mines there are on the up to
8 surrounding tiles.

Step 1: Implement the basic model (6 points)
--------------------------------------------

To implement the game, use the model-view-controller pattern. The controller,
view and a test program (`minesweeper.py`) are already implemented for you.
Your task is to implement the methods of the model, which currently just raise
a `NotImplementedError`. Refer to the docstrings provided to see what the
methods must do.

Hints:

* Think about what information you need to store in the model: which tiles are
  there, which ones have mines, which ones have flags, which ones are revealed.
  Which data structures can you use to store this information? There are many
  possible ways, choose one that you think fits the purpose well.
* Implement an internal method that returns all the up to 8 tiles surrounding
  a given tile. This method will be useful both in `get_status` and in step 2.

Step 2: Automatic revealing (2 points)
--------------------------------------

Most tiles don’t have mines. It is annoying having to click on every single one
of them. For this reason, if a tile is revealed that has 0 surrounding mines,
all surrounding tiles should also be automatically revaled. If one of them has
0 surrounding mines, all tiles surrounding that tile should also be
automatically be revealed. Extend the `reveal` method to implement this
behavior.

Hints:

* This should be solved recursively, i.e. by the `reveal` method calling itself
  again, with different arguments.
* In order to avoid going into an endless loop, check if a tile is already
  revealed before revealing it. If it is already revealed, the method doesn’t
  need to do anything and can return immediately.
* If in step 1 you already wrote a method that returns all the up to 8
  surrounding tiles of a given tile, use it here.

Step 3: Implement an extension (2 points)
------------------------------------------

The game is still missing some bits to make it fun to play. For example:

* There should be some sort of status bar showing you whether you have lost or
  won, and how many mines there are and how many you have already flagged.
* After a game is lost or won, there should be a button to start a new game.
* When a revealed tile shows a number, and on its surrounding tiles there are
  that many flags, a click on the tile should automatically reveal the other,
  non-flagged, tiles.

Choose one of these extensions and implement it. This requires changing not
only the model, but also the view and possibly the controller.

Submission and grading
----------------------

This assignment must be done *individually*.

For this assignment, you can submit up to *3* attempts.

To submit an attempt, add, commit and push all the changes you made, then
*e-mail me `<k.evang@rug.nl>` the URL of your repository*.

*DO NOT FORGET TO E-MAIL ME FOR EACH ATTEMPT, AS I WILL NOT BE CHECKING NESTOR
OR BITBUCKET REGULARLY.*

After each attempt, you will receive a grade and feedback within a few days.

The last possible date for submitting an attempt is *May 20, 2016*.
