import collections
import graphics
import os


RevealAction = collections.namedtuple('RevealAction', ['x', 'y'])
UnflagAction = collections.namedtuple('UnflagAction', ['x', 'y'])
FlagAction = collections.namedtuple('FlagAction', ['x', 'y'])
QuitAction = collections.namedtuple('QuitAction', [])


_TILE_WIDTH = 42
_TILE_HEIGHT = 42
_TILE_SPACE_H = 4
_TILE_SPACE_V = 4


class ImageButton:

    def __init__(self, point):
        self._point = point
        self._image = None

    def update(self, image_filename, win):
        if self._image:
            self._image.undraw()
        self._image = graphics.Image(self._point, image_filename)
        self._image.draw(win)

    def contains(self, point):
        if not self._image:
            return False
        anchor = self._image.getAnchor()
        width = self._image.getWidth()
        height = self._image.getHeight()
        return anchor.x - width / 2 <= point.x <= anchor.x + width / 2 \
            and anchor.y - height / 2 <= point.y <= anchor.y + height / 2


class MinesweeperView:

    def __init__(self, model):
        self._model = model
        self._buttons = {}

    def show(self):
        width = self._model.width * (_TILE_WIDTH + _TILE_SPACE_H) + _TILE_SPACE_H
        height = self._model.height * (_TILE_HEIGHT + _TILE_SPACE_V) + _TILE_SPACE_V
        self._win = graphics.GraphWin('Minesweeper', width, height)
        for y in range(self._model.height):
            for x in range(self._model.width):
                self._buttons[x, y] = ImageButton(graphics.Point(
                    (x + 1) * (_TILE_SPACE_H + _TILE_WIDTH) - _TILE_WIDTH // 2,
                    (y + 1) * (_TILE_SPACE_V + _TILE_HEIGHT) - _TILE_HEIGHT // 2))

    def update(self):
        for y in range(self._model.height):
            for x in range(self._model.width):
                image_filename = os.path.join(os.path.dirname(__file__),
                    'icons', '{}.gif'.format(self._model.get_status(x, y)))
                self._buttons[x, y].update(image_filename, self._win)

    def get_action(self):
        while True:
            try:
                point, button = self._win.getMouse()
            except graphics.GraphicsError:
                return QuitAction()
            if not (self._model.is_game_lost() or self._model.is_game_won()):
                # Go through the buttons to see which one was clicked (if any):
                for y in range(self._model.height):
                    for x in range(self._model.width):
                        if self._buttons[x, y].contains(point):
                            status = self._model.get_status(x, y)
                            if button == 'left':
                                if status == 'blank':
                                    # Left-clicking a blank tile reveals it
                                    return RevealAction(x, y)
                            elif button == 'right':
                                if status == 'flag':
                                    # Right-clicking a flagged tile unflags it
                                    return UnflagAction(x, y)
                                elif status == 'blank':
                                    # Right-clicking a blank tile flags it
                                    return FlagAction(x, y)
