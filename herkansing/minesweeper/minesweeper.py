#!/usr/bin/env python3


import controller
import model
import view


if __name__ == '__main__':
    model = model.MinesweeperModel(8, 8, 10)
    view = view.MinesweeperView(model)
    controller = controller.MinesweeperController(model, view)
    controller.run()
