Resit assignment 2: Dice Game (by Johannes Bjerva)
==================================================

Imagine the following game:

A player has seven dice – one red, one blue and five white.

After rolling the red and blue dice, a 2-digit number is created giving the
following formula:

Number = 10 * (blue die) + (red die)

After this, the player rolls the five white dice. 

The goal is for the player to combine these five dice using the four
mathematical operators `+ - * /` in order to get to this 2-digit number.

The task is to make a text-based game which simulates the rolling of dice, lets
the player play the game, controls the results and gives the user a score based
on time and quality (see below).

One round of the game can look like this:

    Dice rolled:
    Blue = 2
    Red = 5
    White = 2, 4, 4, 1, 3
    
    You need to calculate the number 25 using the numbers 2, 4, 4, 1 and 3,
    using the 4 mathematical operators +, -, * and /.
    
    Your calculation: 4+3*4-1-2
    Huzzah! You made it! Your score is 5.00.
    
    Do you want to play again? (y/n)

The player must input the entire calculation in one row.

The program does not need to take the priority of the operators into account,
but evaluates the expression from left to right. This means that the expression
above is evaluated as:

    4 + 3 * 4 - 1 - 2 = ((((4+3)*4)-1)-2)

Of course, the user will be informed of this before starting the game.

In order to make the game more interesting, you will give the user a score
after each game. This score is dependent on two things – how good the solution
is, and how quickly the player arrives at the solution.

You can calculate the points as follows:

    points = k * time_points * quality_points

where

* `k` is a constant
* `time_points` are calculated as some function such as `1/time` (Hint: Use the
   Python module `time` module for this (`time.time()`))
* `quality_points` are calculated depending on how well the user has played.
   There can be three levels here. For instance:
   * Max score: Each white die is used exactly once
   * Medium score: No white die is used twice, but not each die is used.
   * Low score: One or more white dice are used more than once.

Make sure to test your scoring – for instance, it makes sense if a good
solution which takes a long time to get gives you more points than a bad
solution which is handed in quickly.  Also make sure that faulty calculations
are not accepted by the program.

Submission and grading
----------------------

This assignment must be done *individually*.

For this assignment, you can submit up to *3* attempts.

To submit an attempt, add, commit and push all the changes you made, then
*e-mail me `<k.evang@rug.nl>` the URL of your repository*.

*DO NOT FORGET TO E-MAIL ME FOR EACH ATTEMPT, AS I WILL NOT BE CHECKING NESTOR
OR BITBUCKET REGULARLY.*

After each attempt, you will receive a grade and feedback within a few days.

The last possible date for submitting an attempt is *May 20, 2016*.
