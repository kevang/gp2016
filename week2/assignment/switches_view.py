class TextSwitchesView:

    def show(self):
        pass

    def display(self, model):
        if model.is_switch1_on():
            string = '[ON ]'
        else:
            string = '[OFF]'
        print('{} switch 1'.format(string))
        if model.is_switch2_on():
            string = '[ON ]'
        else:
            string = '[OFF]'
        print('{} switch 2'.format(string))
        if model.is_lightbulb_on():
            string = '[ON ]'
        else:
            string = '[OFF]'
        print('{} lightbulb'.format(string))

    def get_action(self):
        print('Enter 1 to flick switch 1, 2 to flick switch 2, q to quit.')
        while True:
            command = input('> ')
            if command == '1':
                return 'flick_switch1'
            elif command == '2':
                return 'flick_switch2'
            elif command == 'q':
                return 'quit'
            else:
                print('Invalid command!')

    def hide(self):
        pass
