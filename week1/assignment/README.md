Assignment for Week 1
=====================

In this assignment you will practice working with Git and unit tests.

*Read the instructions VERY carefully, there are a number of details you MUST
get right in order to receive points.*

Step 1: Clone the course repository
-----------------------------------

Throughout this course, we will use a Git repository to which I will push all
assignments. First you need a local copy of this repository on the computer you
will do your assignments with (if you want to use multiple computers, you will
need a copy on each computer).

To get a local copy, clone the repository by running the following command on
the command line (do not type the initial dollar sign, it just a convention to
indicate the command prompt):

    $ git clone https://kevang@bitbucket.org/kevang/gp2016.git

This will create a new directory `gp2016` which contains the materials posted
so far. This directory is your copy of the repository. Move it to wherever you
would like to have it.

Step 2: Fork the course repository
----------------------------------

We will also use Git to get your assignment solutions from you to me. For this,
you will need your own private version (“fork”) of the repository on some
server.

There are a number of online services for hosting Git repositories. We will use
[Bitbucket](https://bitbucket.org/) because it offers free private
repositories.

If you do not have a Bitbucket account yet, create one.

Go to https://bitbucket.org/kevang/gp2016/. This is the public repository that
you already cloned.

Click *Fork* in the left menu. In the form that follows, tick the box
*This is a private repository*. Leave everything else unchanged, click
*Fork repository*. Congratulations, you now have your private repository hosted
on Bitbucket!

In order for me to see your homework, I will need read access to your private
repository. Click on the *Send invitation* button on the right and add the user
`kevang`.

The public repository is locally known as `origin`. We now need to make your
private repository locally known as `mine`. To do this, change into your
`gp2016` directory on the command line and run the following command, replacing
`USERNAME` with your Bitbucket username:

    $ git remote add mine https://USERNAME@bitbucket.org/USERNAME/gp2016.git

Step 3: Watch the tests fail
----------------------------

On the command line, change into the `gp2016/week1/assignment` directory. Run
the unit tests:

    python3 -m unittest

You should see the output from Python running a number of tests, and they all
fail. Your task will be to satisfy the requirements checked by the tests until
they all succeed.

Step 4: Satisfy the requirements
--------------------------------

`me.json` is a file in JSON format where you should leave some basic data about
yourself. Specifically, fill in your name and workgroup for this course. Also
add a third property `"Student number" `to the JSON file, with your student
number as value.

Put an image file called `me.jpg`, `me.png` or `me.gif` into the same
directory. Ideally, this should be a photo of your face; this would help us
memorize your name (and remember, it will not be public). However, if you would
rather not submit a picture of yourself, that is also fine. In that case, use
another picture such as a [cartoon of yourself](http://avachara.com/portrait/)
or a picture of a superhero you like.

Finally, add a text file called `me.txt` with a short introduction of yourself,
or a life motto, or a favorite quotation – again, this is intended to help us
get to know you a bit.

Step 5: Make the tests succeed
------------------------------

Run the tests again. If some tests still fail, the messages should give you a
good idea of what is wrong. Fix that and try again until all tests succeed.

Step 6: Apply the style guide
-----------------------------

The directory contains a small Python program `pets.py`. It does not conform to
the [PEP 0008](https://www.python.org/dev/peps/pep-0008/) style guide. Use the
[pep8](https://pypi.python.org/pypi/pep8) command-line tool to style-check the
file and modify it so that `pep8` runs without complaints.

(Note: good coding style is more than making an automatic style checker
succeed. For example, you need to use clear variable names, write clear
comments where appropriate, and sometimes good style even involves breaking the
conventions, as explained in the beginning of PEP 0008. But for this exercise,
shutting `pep8` up is sufficient.)

Step 7: Submit your work
------------------------

Stage all your changes using the `git add` command. Don’t forget to add *both*
the files you modified *and* the new files you added. Double-check with
`git status` that all your changes are staged for commit, and no untracked
files are left.

Commit your changes using `git commit` and a suitable message.

Finally and crucially, *push* your commit to your private repository on
Bitbucket. Note that simply using `git push` won’t work because it will try to
push to `origin`, where you don’t have write permission. Use this command to
push to `mine` instead:

    $ git push mine master

Go to https://bitbucket.org/USERNAME/gp2016/src (change `USERNAME` to your
Bitbucket username) to verify your commits have arrived. *Double-check that all
the changes and new files are there. If not, we will not be able to see them
and you will not receive points.* If something is missing, you may have
forgotten to add, commit or push. Use `git status` to find out what is out of
sync and repeat the necessary steps.

Finally, *submit via Nestor*. All you need to submit is the URL of your private
Bitbucket repository.

Grading
-------

Homework is graded on Tuesdays and Thursdays. 

You may submit up to two attempts. The one with the better grade is counted.

You are strongly encouraged to submit before *Thursday, February 11, 2016,
15:00*. You will then receive a grade on the same day and still have the chance
to submit another attempt if desired.

The last possible date to submit an attempt is *Tuesday, February 16, 2016*.
