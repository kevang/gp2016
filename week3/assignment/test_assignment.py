import ast
import inspect
import json
import thesaurus
import unittest


class ThesaurusTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.t = thesaurus.from_xml(
            '/net/corpora/Cornetto/DATA/v10_cdb_syn.xml')
        cls.synsets = cls.t.data
        with open('sample.json') as f:
            cls.sample = json.load(f)

    def test_data(self):
        self.assertEqual(len(self.synsets), 70434)
        self.assertEqual(self.synsets[0]['synset_id'], 'd_n-39469')
        self.assertEqual(self.synsets[0]['definition'],
            'heimelijk bespieder;iemand die mensen bespiedt;')
        self.assertEqual(len(self.synsets[0]['words']), 2)
        self.assertEqual(self.synsets[0]['words'][0], 'gluurder')
        self.assertEqual(self.synsets[0]['words'][1], 'voyeur')
        self.assertEqual(self.synsets[2]['synset_id'], 'd_n-39467')
        self.assertEqual(self.synsets[2]['definition'], None)
        self.assertEqual(len(self.synsets[2]['words']), 1)
        self.assertEqual(self.synsets[2]['words'][0], 'glissando')
        self.assertEqual(self.synsets[-1]['synset_id'], 'd_n-11517')
        self.assertEqual(self.synsets[-1]['definition'],
            'binnenkant v.d. hand;binnenkant v.d. hand;')
        self.assertEqual(len(self.synsets[-1]['words']), 2)
        self.assertEqual(self.synsets[-1]['words'][0], 'handpalm')
        self.assertEqual(self.synsets[-1]['words'][1], 'palm')

    def test_lookup_simple(self):
        result = self.t.get_synsets('deken')
        self.assertEqual(result, self.sample[:4])

    def test_lookup_index(self):
        result = self.t.synsets_by_word['deken']
        self.assertEqual(result, self.sample[:4])
        result = self.t.get_synsets2('deken')
        self.assertEqual(result, self.sample[:4])


class JSONExportTest(unittest.TestCase):

    def test_json_export(self):
        t = thesaurus.from_json('sample.json')
        t.to_json('sample.exported.json')
        u = thesaurus.from_json('sample.exported.json')
        self.assertEqual(t.data, u.data)


class ListCompTest(unittest.TestCase):

    def test_listcomp(self):
        tree = ast.parse(inspect.getsource(thesaurus))
        listcomp_count = count_nodes(tree, ast.ListComp)
        self.assertGreaterEqual(listcomp_count, 3, 'There should be at least '
            'three list comprehensions in the thesaurus module (see steps 2, 4'
            ' and 6)')


def count_nodes(node, node_class):
   """
   node is an AST node. node_class is a node class as exported by the ast
   module. Returns how many nodes of this class occur in node. Nodes that are
   descendants of nodes of the same class are not counted.
   """
   if isinstance(node, node_class):
       return 1
   if isinstance(node, list):
       return sum(count_nodes(elem, node_class) for elem in node)
   if isinstance(node, ast.AST):
       return sum(count_nodes(node.__dict__[field], node_class) for field in
                  node._fields)
   return 0
