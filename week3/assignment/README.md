Assignment for week 3
=====================

In this assignment you will create an interactive
[Thesaurus](https://en.wikipedia.org/wiki/Thesaurus) application where the user
enters a Dutch word and gets back a definition for every sense of the word,
along with a list of synonyms. You will practice working with XML, JSON,
dictionaries and list comprehensions.

Step 1: Get the starter code
----------------------------

On the command line, go into your local copy of the course repository. Update
it using the following command.

    git pull

This adds a directory `week3`, containing slides and assignment files.

In `week3/assignment`, you will find a file called `sample.json` and a script
called `thesaurus.py`. Have a look at `sample.json`. It contains, in JSON
format, a short list of _synsets_. Synsets are sets of words with the same
meaning, e.g. the Dutch words _vorst_ and _heer_ could be in the same synset
with the meaning “ruler”. Words with multiple meanings can be found in multiple
synsets, e.g. _vorst_ also occurs in another synset with the meaning “frost”.
In the JSON file, each synset is represented as an object with three
attributes: `synset_id` (an ID unique for each synset), `definition` (a string
containing a short definition of the meaning) and `words` (a list of words that
are in this synset).

In `thesaurus.py`, a class `Thesaurus` is defined. Internally, it uses a data
structure that corresponds directly to the format used in the JSON file, as
this code demonstrates:

    $ python3
    Python 3.4.3 (default, Oct 14 2015, 20:28:29) 
    [GCC 4.8.4] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import syndict
    >>> d = syndict.from_json('sample.json')
    >>> d._data
    [{'synset_id': 'd_n-38873', 'words': ['deken', 'beddedeken', 'beddendeken', 'dek'], 'definition': 'warm kleed om onder te slapen;rechthoekig kleed tot beschutting tegen de kou;'}, {'synset_id': 'n_n-506560', 'words': ['deken'], 'definition': 'hoofd van een kapittel van kanunniken;'}, {'synset_id': 'n_n-506561', 'words': ['deken'], 'definition': 'priester v.e. aantal parochies;'}, {'synset_id': 'n_n-506559', 'words': ['deken'], 'definition': 'van de Orde van Advocaten;'}, {'synset_id': 'd_n-40638', 'words': ['matras'], 'definition': None}, {'synset_id': 'd_n-35027', 'words': ['matras', 'vlooienbunker'], 'definition': 'meestal zachte, onderste laag v.e. bed;'}]

Step 2: Read thesaurus from XML (4 points)
------------------------------------------

`sample.json` only contains a few synsets, a more complete thesaurus called
Cornetto is available in XML format from `/net/corpora`.

If you are working on a lab computer, make a symbolic link to Cornetto inside
your `week3/assignment` directory:

    $ ln -s /net/corpora/net/corpora/Cornetto/DATA/v10_cdb_syn.xml

The symbolic link allows you to work with the file as if it was in this
directory, without it occupying extra hard disk capacity.

If you are working on your own computer, copy the actual XML file from the
university network into your `week3/assignment` directory.

In both cases, `git status` will ignore the file because the contents of the
hidden file `.gitignore` tell it to do so. This way, you will not accidentally
add and commit it (Git is not very suitable for handling large files).

Add a function `from_xml` to the `thesaurus` module that takes the path to such
a file, reads it and returns a `Thesaurus` object with the same internal data
representation as above: a list of dictionaries with keys `'synset_id'`,
`'definition'` and `'words'`.

Requirements:

* Use `xml.etree.ElementTree` to parse the XML file
* For each synset, use a list comprehension to convert the children of the
  `<synonyms>…</synonyms>` elements into a list of strings.

Step 3: Add JSON export method (1 point)
----------------------------------------

Add a method `to_json` to the `Thesaurus` class. It should take a file name
argument and write the internal data of the `Thesaurus` object to that file, in
the same format as `sample.json`.

You can test if your JSON export method works by running `python3`
interactively and using it to convert the XML file to JSON.

Step 4: Add simple synset lookup (1 point)
-------------------------------------------

`thesaurus.py` is intended to be run as an interactive thesaurus. However, this
code does not work yet because the `Thesaurus` class is still missing the
method `get_synsets`. This method should take a word as argument and return all
synsets containing this word.

Implement the `get_synsets` method. Its body should only consist of a single
statement `return X` where `X` is a list comprehension.

Verify that you can now use the interactive thesaurus (make sure you have at
least 1 GiB of free RAM first, and be patient as reading the XML can take a
while):

    python3 -m thesaurus
    Enter a word: agent
    
    Definition: iem. zonder bepaalde titel of rang in diplomatieke of politieke dienst;
    Synonyms:   agent
    
    Definition: iemand die aan winkels verkoopt;vertegenwoordiger v.d. bedrijf;vertegenwoordiger;
    Synonyms:   agent vertegenwoordiger factoor factor handelsagent handelsreiziger
    
    Definition: iemand in dienst bij de politie;politiebeambte;politieagent;politieagent;politieman;geheel aan ambtenaren v.d. openbare orde;iemand in dienst bij de politie;politieagent;politieagent;politieagent;bepaald kaartspel;
    Synonyms:   agent politieagent bout diender flic gerechtsdienaar glimmerik juut klabak politie politieambtenaar politiebeambte rakker sjouter smeris tuut wout flik pandoer

    Enter a word:

Step 5: Add dictionary-based synset lookup (3 points)
-----------------------------------------------------

While elegant, `get_synsets` is not very efficient. It has to go through the
entire long list of synsets and check each single one. It would be more
efficient to use a dictionary for lookup.

Add some code to the constructor of `Thesaurus` that creates a `defaultdict`.
This dictionary should have words as keys. The value for each word should be
the list of synsets (represented as `dict` objects) that contain this word, so
that the synsets for each word are easy to look up.

Add a method `get_synsets2` that is like `get_synsets`, except it uses the
dictionary to find the list of synsets to return. Change the `__main__` code to
use `get_synsets2` instead of `get_synsets`.

Step 6: Filter list of synonyms (1 point)
-----------------------------------------

Currently our application shows all words of each synset it shows, also the one
entered by the user. Since it does not make much sense to show a word as a
synonym of itself, change the `__main__` code to remove it for display, using a
list comprehension.

Step 7: Run the unit tests
--------------------------

From the `week3/assignment` directory, run the following command:

    python3 -m unittest

If you completed steps 2–6, all tests should pass. Note that additional tests
may be used to grade your work.

Step 8: Submit your work
------------------------

From the `week3/assignment` directory, run `git status` to see all the files
you modified and created, except the ignored ones. Only `thesaurus.py` should
be shown as modified. Add it. Commit the changes with a meaningful message.
Push them to your private repository using this command:

    git push mine master

Go to https://bitbucket.org/USERNAME/gp2016/src/master/week2/assignment/
(substituting your own username) and check the files to verify all your
changes have arrived (you may have to login to BitBucket first).

Finally, submit via Nestor. Again, you do not have to attach any files, just
submit the URL of your private BitBucket repository.

Grading
-------

Homework is graded twice a week, usually on Tuesdays and once during the
weekend.

You may submit up to two attempts. The one with the better grade is counted.

You are strongly encouraged to finish the assignment and submit as early as
possible. This way, you will receive a grade for your first attempt and still
have time to submit another attempt if desired.

The last possible date to submit an attempt is *Tuesday, March 1, 2016*.

*Happy hacking!*
