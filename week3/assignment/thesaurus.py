#!/usr/bin/env python3


import json


class Thesaurus:

    def __init__(self, data):
        self.data = data


def from_json(json_filename):
    with open(json_filename) as f:
        return Thesaurus(json.load(f))


if __name__ == '__main__':
    t = from_xml('v10_cdb_syn.xml')
    while True:
        try:
            word = input('Enter a word: ')
        except EOFError:
            break
        if not word:
            break
        synsets = t.get_synsets(word)
        if not synsets:
            print()
            print('Word not found.')
            print()
            continue
        for synset in synsets:
            print()
            print('Definition: {}'.format(synset['definition']))
            print('Synonyms:   {}'.format(synset['words']))
        print()
